const _ = require('underscore');
const file = require('fs-extra');

function validate(options) {
  const {
    CWD, 
    configFilename, 
    configPath, 
    config, 
    flows, 
    resultsFilePath, 
    resultsDir, 
    resultsFileJSON, 
    browserInstance, 
    viewports
  } = require("./common")(options).getOptions;
  _.each(resultsFileJSON, function(result) {
    if(!result.reviewed) {
      console.error('You have unreviewed changes.');
      process.exit(1)
    }
  });
  
  console.log('All changes have been reviewed.')
}

module.exports = function(options) {
  return validate(options)
}