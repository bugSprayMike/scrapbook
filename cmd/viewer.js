const _ = require('underscore');
const md5 = require('md5');
const file = require('fs-extra');

function viewer(options) {
  const {
    CWD, 
    configFilename, 
    configPath, 
    config, 
    flows, 
    resultsFilePath, 
    resultsDir, 
    resultsFileJSON, 
    browserInstance, 
    viewports
  } = require("./common")(options).getOptions;
  
  var errorsFound = false;
  
  _.each(resultsFileJSON, function(result) {
    if(!result.reviewed) {
      errorsFound = true;
    } 
  });
  
  if(!errorsFound) {
    console.log('No changes to review at this time');
    process.exit();
  }
  
  
  function saveChanges(changes) {
    file.writeFile(resultsFilePath, JSON.stringify(changes), 'utf8', function(e) {
      return e;
    })
  }
  
  require('./viewer/api')(config, resultsFileJSON, flows, resultsDir, saveChanges);
}

module.exports = function(options) {
  viewer(options);
}