module.exports = function(options) {
  return {
    getOptions: getPaths(options)
  }
}

function getPaths(options) {
  const process = require('process');
  const CWD = options.cwd ? process.cwd() + '/' + options.cwd : process.cwd() + '/';
  const configFilename = options.config ? options.config : "storybook"
  const configPath = options.configPath ? options.configPath + configFilename : CWD + configFilename;

  try {
    require(configPath);
  } catch (e) {
    return {
      error: "Config file path not found"
    };
  }
  
  const config = require(configPath)
  const flows = require(CWD + config.flows[0].source); //accept steps in json format
  
  try {
    require(CWD + config.results.dir + '/' + config.results.reportFile);
  } catch (e) {
    return {
      error: "Flow file path not found"
    };
  }
  
  
  const resultsFilePath = CWD + config.results.dir + '/' + config.results.reportFile; //accept last run results

  try {
    require(resultsFilePath);
  } catch (e) {
    return {
      error: "Results file path not found"
    };
  }
  
  const resultsDir = CWD + config.results.dir; //accept last run results
  const resultsFileJSON = require(resultsFilePath); //accept last run results
  
  
  const browserInstance = "http://localhost:9222" || process.env.chromeInstance;
  const viewports = config.viewPorts;
  
  return {
    CWD, 
    configFilename, 
    configPath, 
    config, 
    flows, 
    resultsFilePath, 
    resultsDir, 
    resultsFileJSON, 
    browserInstance, 
    viewports
  }
}