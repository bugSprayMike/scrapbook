const express = require('express')
const app = express()
const path = require('path');
const process = require('process');
const bodyParser = require('body-parser');

function startExpress(config, results, flows, resultsDir, saveChanges) {
  console.log(resultsDir);
  app.use(express.static(path.join(__dirname, 'public')));
  app.use('/report', express.static(resultsDir));
  app.use( bodyParser.json() );       // to support JSON-encoded bodies
  app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
  }));


  app.get('/config', function (req, res) {
    res.json(config)
  })

  app.get('/flows', function (req, res) {
    res.json(flows)
  })

  app.get('/results', function (req, res) {
    res.json(results)
  })

  app.post('/results', function (req, res) {
    saveChanges(req.body);
    res.json({success:true})
  })
  
  app.listen(8010, function () {
    const opn = require('opn')
    opn('http://localhost:8010')
  })
}

module.exports = startExpress;