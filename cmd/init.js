const _ = require('underscore');
const file = require('fs-extra');
const process = require('process');

function init(options) {
  file.copySync('./demo/', process.cwd()+"/scrapbook");
}

module.exports = function(options) {
  return init(options)
}