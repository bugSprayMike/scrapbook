const CDP = require('chrome-remote-interface');
const ProgressBar = require('progress');
const _ = require('underscore');
const md5 = require('md5');
const file = require('fs-extra');


var changes = {} //keep track of snapshots that have a different hash

module.exports = function(cmd) {
  const {
    CWD, 
    configFilename, 
    configPath, 
    config, 
    flows, 
    resultsFilePath, 
    resultsDir, 
    resultsFileJSON, 
    browserInstance, 
    viewports
  } = require("./common")(cmd).getOptions;
  

  if(config.startScript) {
    const { spawn } = require('child_process');
    const backgroundService = spawn(config.startScript[0], config.startScript[1], {env: Object.assign(process.env, config.startScript[2])});

    backgroundService.stdout.on('data', (data) => {
      console.log(data);
    });
  }

  setTimeout(function() {
    CDP(async function(client) {
      const CLIENT = client;
      initServices(CLIENT, false);
      setUpViewport(CLIENT, viewports.desktop);
      const {frameId} = await CLIENT.Page.navigate({url: config.url});
      var loaded = false;
      
      // page load event
      CLIENT.Page.loadEventFired(async () => {
        if(loaded) {
          return false;
        }
        loaded = true;
        hideScrollbars(CLIENT, frameId);
        runTestFlows(CLIENT, viewports, flows, {resultsFileJSON, resultsFilePath, resultsDir}, changes);
      });
    }).on('error', err => {
      console.log(err)
      exit(CLIENT, err);
    });
  }, 4000)
  
  
  
  
  /***
   * @function: runTestFlows
   * @description: loop thru steps, take snapshots and compare
   **/
  function runTestFlows(CLIENT, viewports, flows, results) {
    var barOpts = {
      width: 20,
      total: flows.length,
      clear: true
    };
    var bar = new ProgressBar(' running [:bar] :percent :etas', barOpts);
    
    _.each(viewports, async function(viewport) {  
      recursiveSteps(CLIENT, bar, flows, viewport, 0, results);
    })
  }

  async function recursiveSteps(CLIENT, bar, flows, viewport, step, results) {
    bar.tick(step + 1);
    
    if(flows[step].script) {
      CLIENT.Runtime.evaluate({expression: flows[step].script});
    }
    
    if(flows[step].exec) {
        //@TODO figure out how to execute commands on the CDP side
    }
    
    if(flows[step].snapshot) {
      var resultObject = results.resultsFileJSON[flows[step].snapshot.stepId];
      var lasthash = resultObject ? resultObject.hash : "";
      var screenshot = await snapshot(CLIENT, lasthash);
      
      if(screenshot) {
        changes[flows[step].snapshot.stepId] = {
          hash: screenshot.hash,
          reviewed: false,
          updated: new Date().getTime()
        };
        writeScreenshot(screenshot.buffer, screenshot.hash, flows[step].snapshot.stepId, results);
      }
    }
    
    step = step + 1;
    
    if(step < flows.length) {
      setTimeout(function() {
        recursiveSteps(CLIENT, bar, flows, viewport, step, results);
      }, flows[step-1].wait ? flows[step-1].wait : 0);
    }else {
      if(changes) {
        file.writeFile(results.resultsFilePath, JSON.stringify(changes), 'utf8', function(e) {
          exit(CLIENT, 0);
        })
      }
    }
  };


  /******************************************************************************
   * General purpose functions
   *****************************************************************************/
  async function initServices(CLIENT, userAgent) {
   // Enable events on domains we are interested in.
   await CLIENT.Page.enable();
   await CLIENT.DOM.enable();
   await CLIENT.CSS.enable();
   await CLIENT.Network.enable();
   await CLIENT.Runtime.enable();
   
   // If user agent override was specified, pass to Network domain
   if (userAgent) {
     await CLIENT.Network.setUserAgentOverride({userAgent});
   }
  }


  /***
   * @function: hideScrollbars
   * @description: when page renders hide scrollbar
   **/
  async function hideScrollbars(CLIENT, frameId) {
    // Add a CSS rule that hides scrollbars in screenshot
    const {styleSheetId} = await CLIENT.CSS.createStyleSheet({frameId: frameId});
    await CLIENT.CSS.addRule({
      styleSheetId: styleSheetId,
      ruleText: '::-webkit-scrollbar { display: none; }',
      location: {startLine: 0, startColumn: 0, endLine: 0, endColumn: 0},
    });
  }



  /***
   * @function: setUpViewport
   * @description: set up browser viewport
   **/
  async function setUpViewport(CLIENT, deviceMetrics) {
    // Set up viewport resolution, etc.
    await CLIENT.Emulation.setDeviceMetricsOverride(deviceMetrics);
    await CLIENT.Emulation.setVisibleSize(deviceMetrics);
  }



  /***
   * @function: snapshot
   * @description: take a snapshot of the current dom and compares 
   * to the last execution to detect changes.
   **/
  async function snapshot(CLIENT, lasthash, documentNodeId) {
    //check that results are different
    if (documentNodeId) {
      const {root: {nodeId: documentNodeId}} = await CLIENT.DOM.getDocument();
      const {nodeId: bodyNodeId} = await CLIENT.DOM.querySelector({
        selector: 'body',
        nodeId: documentNodeId,
      });
      const {model: {height}} = await CLIENT.DOM.getBoxModel({nodeId: bodyNodeId});

      await CLIENT.Emulation.setVisibleSize({width: viewports.desktop.width, height: viewports.desktop.height});
      await CLIENT.Emulation.forceViewport({x: 0, y: 0, scale: 1});
    }
    
    const screenshot = await CLIENT.Page.captureScreenshot({format:'png'});
    const buffer = new Buffer(screenshot.data, 'base64');
    const hash = md5(buffer);
    
    if(hash === lasthash) {
      return false;
    }
    
    return {buffer, hash};
  }



  /***
   * @function: writeScreenshot
   * @description: save changed results to the filesystem
   **/
  function writeScreenshot(buffer, hash, stepId, results) {
    //save the reports.json file
    //save the file output
    const filename = stepId + '-' + hash + '.png';
    
    file.outputFile(results.resultsDir + '/' + filename, buffer, 'base64', function(err) {
      if (err) {
        console.error(err);
      }
    });
  }

  function writeChanges(results, changes) {
    file.writeJson(results.resultsFilePath, changes, function(e) {
      console.log('save results', e);
    })
  }
}


/***
 * @function: exit
 * @description: log results and exit with appropriate exit code
 **/
function exit(CLIENT, error) {
  if(CLIENT) {
    CLIENT.close();
  }
  
  var exitCode = _.keys(changes).length ? 1 : 0;
  
  if(error) {
    console.error(error);
    process.exit(1);
  }
  
  if(exitCode) {
    console.log(changes);
  }
  
  process.exit(exitCode);
}

