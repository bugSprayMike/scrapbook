# [ScrapBook](https://gitlab.com/bugSprayMike/scrapbook)

ScrapBook is a visual regression suite for testing web based user interfaces. Scrapbook will run user defined test scripts and take snapshots. These snapshots are compared over time to detect unintentional changes. 

## Getting Started

### Install the module
`npm install scrapbook -G`


### Generate a configuration file
`scrapbook init`

This will output a `scrapbook.json` file that contains properties describing the test runtime.



### Run the test
`scrapbook run`

This will output a `results.json` file containing the hash of screenshot data and whether the change has been approved or not. 

Running `scrapbook validate` will ensure that all changes have been approved.

### Review the changes
`scrapbook review`

This will launch a UI to view what has changed and to accept or deny the differences.

## Configuration Settings

```json
{
	"url": "http://example.com",
	"flows": [{
		"name": "Default user flow",
		"source": "./steps.json",
		"viewport": ["desktop", "mobile"]
	}],
	"results": {
		"dir": "reports",
		"reportFile": "report.json"
	},
	"viewPorts": {
		"desktop": {
	    "width": 1400,
	    "height": 1900,
	    "deviceScaleFactor": 0,
	    "mobile": false,
	    "fitWindow": false
	  }
	}
}

```

## Defining user flows

### Sample flow
```json
[
  {
    "script": "window.document.body.style.backgroundColor = \"red\";",
    "description": "turn the background red",
    "wait": 1000
  },
  {
    "snapshot" : {
      "description" : "ensure background color is red",
      "ticketId": "1234",
      "stepId": "step1"
    }
  },
  {
    "snapshot" : {
      "domId" : "domId",
      "description" : "what is being tested",
      "ticketId": "1234",
      "stepId": "step2"
    }
  },
  {
    "wait": 1000
  }
]
```

## Steps
User flows are defined as arrays of user steps and run synchronously. Steps can be a `script`, `snapshot`, or `exec`. 

### Script
Scripts are plain javascript that are used to interact with the DOM. 

### Exec
Exec is used to run steps that interact with the browser and attributes of the Chrome Debugger Protocol.

### Snapshot
Snapshots are used to compare the visual state of the UI. 

