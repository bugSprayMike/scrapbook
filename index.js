#!/usr/bin/env node
const program = require('commander');
const process = require('process');

program
  .version(require('./package').version);

program
  .command('init')
  .description('Run vizual regression suite using local scrapbook.json file')
  .option("-d, --debugger <port>", "Which chrome debugger port to use")
  .option("-f, --frontend <url>", "The base URL of the UI to be tested")
  .option("-w, --cwd <directory>", "The current working directory")
  .option("-c, --config <file>", "The config file name. Defaults to scrapbook.json")
  .action(function(cmd, options){
    require('./cmd/init')(cmd);
  }).on('--help', function() {
    console.log('  Examples:');
    console.log();
    console.log('    $ storybook init');
    console.log();
  });

program
  .command('run')
  .description('Run vizual regression suite using local scrapbook.json file')
  .option("-d, --debugger <port>", "Which chrome debugger port to use")
  .option("-f, --frontend <url>", "The base URL of the UI to be tested")
  .option("-w, --cwd <directory>", "The current working directory")
  .option("-c, --config <file>", "The config file name. Defaults to scrapbook.json")
  .action(function(cmd, options){
    // run(cmd);
    require("./cmd/run")(cmd)
  }).on('--help', function() {
    console.log('  Examples:');
    console.log();
    console.log('    $ storybook run');
    console.log();
  });

program
  .command('review')
  .description('View and approve changes that were detected in the build')
  .option("-p, --port <port>", "Which port to serve the viewer UI from")
  .option("-w, --cwd <directory>", "The current working directory")
  .option("-c, --config <file>", "The config file name. Defaults to scrapbook.json")
  .action(function(cmd, options){
    require('./cmd/viewer')(cmd);
  }).on('--help', function() {
    console.log('  Examples:');
    console.log();
    console.log('    $ scrapbook review');
    console.log();
  });

program
  .command('validate')
  .description('Run vizual regression suite using local scrapbook.json file')
  .option("-d, --debugger <port>", "Which chrome debugger port to use")
  .option("-f, --frontend <url>", "The base URL of the UI to be tested")
  .option("-w, --cwd <directory>", "The current working directory")
  .option("-c, --config <file>", "The config file name. Defaults to scrapbook.json")
  .action(function(cmd, options){
    require('./cmd/validate')(cmd);
  }).on('--help', function() {
    console.log('  Examples:');
    console.log();
    console.log('    $ storybook validate');
    console.log();
  });

program.parse(process.argv);